import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
/**
 *
 * @author Axel Tamayo
 */
public class Taller3InterfasDeusuario extends Application {
    @Override
    public void start(Stage primaryStage) {
        Button closeButton = new Button("Button");
        CheckBox checkbox = new CheckBox("CheckBox");
        Hyperlink hyperlink = new Hyperlink("Hyperlink");
        ToggleButton togglebutton = new ToggleButton("ToggleButton");
        RadioButton radiobutton = new RadioButton("RadioButton");
        Label label = new Label("Label");
        TextField textField = new TextField();
        
        Label label1 = new Label("Button:");
        Label label2 = new Label("CheckBox:");
        Label label3 = new Label("Hyperlink:");
        Label label4 = new Label("ToggleButton:");
        Label label5 = new Label("RadioButton:");
        Label label6 = new Label("Label:");
        Label label7 = new Label("Textfield:");

        VBox vbox = new VBox(label1, label2, label3, label4, label5, label6, label7);
        vbox.setSpacing (20);
        label1.setGraphic(closeButton);
        label1.setContentDisplay(ContentDisplay.RIGHT);
        label2.setGraphic(checkbox);
        label2.setContentDisplay(ContentDisplay.RIGHT);
        label3.setGraphic(hyperlink);
        label3.setContentDisplay(ContentDisplay.RIGHT);      
        label4.setGraphic(togglebutton);
        label4.setContentDisplay(ContentDisplay.RIGHT);
        label5.setGraphic(radiobutton);
        label5.setContentDisplay(ContentDisplay.RIGHT);
        label6.setGraphic(label);
        label6.setContentDisplay(ContentDisplay.RIGHT);
        label7.setGraphic(textField);
        label7.setContentDisplay(ContentDisplay.RIGHT);

        Scene scene = new Scene(vbox, 400, 300);     
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    public static void main(String[] args) {
        launch(args);
    }
}


